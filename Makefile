download:
	curl -OL https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar && pwd
	
codestyle:
	php phpcs.phar web/ --standard=PSR2

