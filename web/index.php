<?php

//показывать ip инстанса backend
$text = file_get_contents('http://2ip.ru');
preg_match_all('#<big id="d_clip_button">(.+)</big>#iU', $text, $r);
$text=$r[1][0];
echo ("backend-server IP: ".$text."<br>");

// подключаем скрипт соединения с db
require_once 'connection.php';

// подключаемся к серверу
$link = mysqli_connect($host, $user, $password, $database);
if ($link == false) {
    print("1) ОШИБКА: Нет подключения к серверу MySQL.<br>".mysqli_connect_error($link));
} else {
    print "1) Соединение c сервером MySQL успешно установлено. <br>";
}

//увеличить значение на +1
$query = "UPDATE counters SET value=value+1 where name='visits'";
$result3 = mysqli_query($link, $query);
if ($result3 == false) {
    print("2) ОШИБКА: Значение в таблице не обновленно. <br>");
} else {
    print "2) Значение в таблице обновленно. <br>";
}

// запрос значений таблицы
$query ="SELECT * FROM counters where name='visits'";
$result = mysqli_query($link, $query) or die("3) ОШИБКА запроса.<br>".mysqli_error($link));
if ($result) {
    echo "3) Значение таблицы считанно: ";
}

// вывод значений таблицы
$item = mysqli_fetch_array($result);
    print($item['value'] . "<br>");

// Освобождаем память от результата
mysql_free_result($result);

// закрываем подключение
mysqli_close($link);
